package compiler;

import java.io.PrintStream;

public class Memory {

	private final PrintStream printStream;
	private final byte[] data = new byte[65535];
	private int pointer = 0;

	public Memory(PrintStream printStream) {
		this.printStream = printStream;
	}

	public byte GetCurrentCell() {
		return this.data[this.pointer];
	}

	public void Print() {
		this.printStream.print((char) this.data[this.pointer]);
	}

	public void IncreaseCell() {
		this.data[this.pointer] += 1;
	}

	public void DecreaseCell() {
		this.data[this.pointer] -= 1;
	}

	public void IncreasePointer() {
		this.pointer += 1;
	}

	public void DecreasePointer() {
		this.pointer -= 1;
	}
}
