package compiler.commandParsers;

import compiler.BrainfuckCommand;
import compiler.Memory;
import compiler.commandFactories.DecreasePointerCommandFactory;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

@BrainfuckCommand(character = '<')
public class DecreasePointerCommandParser extends ACommandParser {
	public DecreasePointerCommandParser(Memory memory, List<Character> code) throws NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
		super(memory, code);
	}

	@Override
	void parse() {
		this.getCode().remove(0);
		this.setCommandFactory(new DecreasePointerCommandFactory(this.getMemory()));
	}
}
