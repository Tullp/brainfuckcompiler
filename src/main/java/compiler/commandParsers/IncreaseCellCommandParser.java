package compiler.commandParsers;

import compiler.BrainfuckCommand;
import compiler.Memory;
import compiler.commandFactories.IncreaseCellCommandFactory;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

@BrainfuckCommand(character = '+')
public class IncreaseCellCommandParser extends ACommandParser {
	public IncreaseCellCommandParser(Memory memory, List<Character> code) throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
		super(memory, code);
	}

	@Override
	void parse() {
		this.getCode().remove(0);
		this.setCommandFactory(new IncreaseCellCommandFactory(this.getMemory()));
	}
}
