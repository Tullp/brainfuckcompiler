package compiler.commandParsers;

import compiler.BrainfuckCommand;
import compiler.Memory;
import compiler.commandFactories.IncreasePointerCommandFactory;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

@BrainfuckCommand(character = '>')
public class IncreasePointerCommandParser extends ACommandParser {
	public IncreasePointerCommandParser(Memory memory, List<Character> code) throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
		super(memory, code);
	}

	@Override
	void parse() {
		this.getCode().remove(0);
		this.setCommandFactory(new IncreasePointerCommandFactory(this.getMemory()));
	}
}
