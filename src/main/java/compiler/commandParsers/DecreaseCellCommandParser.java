package compiler.commandParsers;

import compiler.BrainfuckCommand;
import compiler.Memory;
import compiler.commandFactories.DecreaseCellCommandFactory;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

@BrainfuckCommand(character = '-')
public class DecreaseCellCommandParser extends ACommandParser {
	public DecreaseCellCommandParser(Memory memory, List<Character> code) throws NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
		super(memory, code);
	}

	@Override
	void parse() {
		this.getCode().remove(0);
		this.setCommandFactory(new DecreaseCellCommandFactory(this.getMemory()));
	}
}
