package compiler.commandParsers;

import compiler.BrainfuckCommand;
import compiler.Invoker;
import compiler.Memory;
import compiler.Parser;
import compiler.commandFactories.LoopCommandFactory;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

@BrainfuckCommand(character = '[')
public class LoopCommandParser extends ACommandParser {

	public LoopCommandParser(Memory memory, List<Character> code) throws NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
		super(memory, code);
	}

	@Override
	void parse() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
		this.getCode().remove(0);
		Invoker invoker = new Parser(this.getCode(), this.getMemory()).getInvoker((code) -> code.get(0) == ']');
		this.getCode().remove(0);
		this.setCommandFactory(new LoopCommandFactory(this.getMemory(), invoker));
	}
}
