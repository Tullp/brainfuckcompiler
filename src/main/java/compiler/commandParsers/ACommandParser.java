package compiler.commandParsers;

import compiler.Memory;
import compiler.commandFactories.ACommandFactory;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

public abstract class ACommandParser {
	private final Memory memory;
	private final List<Character> code;
	private ACommandFactory commandFactory;

	public ACommandParser(Memory memory, List<Character> code) throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
		this.memory = memory;
		this.code = code;

		this.parse();
	}

	abstract void parse() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException;

	public Memory getMemory() { return this.memory; }
	public List<Character> getCode() { return this.code; }
	public ACommandFactory getCommandFactory() { return this.commandFactory; }
	public void setCommandFactory(ACommandFactory commandFactory) { this.commandFactory = commandFactory; }

}
