package compiler.commandParsers;

import compiler.BrainfuckCommand;
import compiler.Memory;
import compiler.commandFactories.PrintCommandFactory;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

@BrainfuckCommand(character = '.')
public class PrintCommandParser extends ACommandParser {

	public PrintCommandParser(Memory memory, List<Character> code) throws NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
		super(memory, code);
	}

	@Override
	void parse() {
		this.getCode().remove(0);
		this.setCommandFactory(new PrintCommandFactory(this.getMemory()));
	}
}
