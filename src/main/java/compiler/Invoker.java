package compiler;

import compiler.commands.ACommand;

import java.util.ArrayList;
import java.util.List;

public class Invoker {

	private final List<ACommand> commands;

	public Invoker() {
		this.commands = new ArrayList<>();
	}

	public void addCommand(ACommand command) {
		this.commands.add(command);
	}

	public void invoke() {
		for (ACommand command : this.commands)
			command.Execute();
	}
}
