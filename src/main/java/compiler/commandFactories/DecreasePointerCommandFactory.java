package compiler.commandFactories;

import compiler.Memory;
import compiler.commands.ACommand;
import compiler.commands.DecreasePointerCommand;

public class DecreasePointerCommandFactory extends ACommandFactory {
	public DecreasePointerCommandFactory(Memory memory) { super(memory); }

	@Override
	public ACommand CreateCommand() {
		return new DecreasePointerCommand(this.getMemory());
	}
}
