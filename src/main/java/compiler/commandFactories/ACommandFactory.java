package compiler.commandFactories;

import compiler.Memory;
import compiler.commands.ACommand;

public abstract class ACommandFactory {
	private final Memory memory;
	public ACommandFactory(Memory memory) { this.memory = memory; }
	public Memory getMemory() { return this.memory; }
	abstract public ACommand CreateCommand();
}
