package compiler.commandFactories;

import compiler.Memory;
import compiler.commands.ACommand;
import compiler.commands.IncreasePointerCommand;

public class IncreasePointerCommandFactory extends ACommandFactory {
	public IncreasePointerCommandFactory(Memory memory) { super(memory); }

	@Override
	public ACommand CreateCommand() {
		return new IncreasePointerCommand(this.getMemory());
	}
}
