package compiler.commandFactories;

import compiler.Memory;
import compiler.commands.ACommand;
import compiler.commands.DecreaseCellCommand;

public class DecreaseCellCommandFactory extends ACommandFactory {
	public DecreaseCellCommandFactory(Memory memory) { super(memory); }

	@Override
	public ACommand CreateCommand() {
		return new DecreaseCellCommand(this.getMemory());
	}
}
