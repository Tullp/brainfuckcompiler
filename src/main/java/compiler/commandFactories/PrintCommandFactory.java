package compiler.commandFactories;

import compiler.Memory;
import compiler.commands.ACommand;
import compiler.commands.PrintCommand;

public class PrintCommandFactory extends ACommandFactory {
	public PrintCommandFactory(Memory memory) { super(memory); }

	@Override
	public ACommand CreateCommand() {
		return new PrintCommand(this.getMemory());
	}
}
