package compiler.commandFactories;

import compiler.Invoker;
import compiler.Memory;
import compiler.commands.ACommand;
import compiler.commands.LoopCommand;

public class LoopCommandFactory extends ACommandFactory {

	private final Invoker invoker;
	public LoopCommandFactory(Memory memory, Invoker invoker) {
		super(memory);
		this.invoker = invoker;
	}

	@Override
	public ACommand CreateCommand() {
		return new LoopCommand(this.getMemory(), this.invoker);
	}
}
