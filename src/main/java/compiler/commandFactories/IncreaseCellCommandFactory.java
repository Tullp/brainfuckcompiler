package compiler.commandFactories;

import compiler.Memory;
import compiler.commands.ACommand;
import compiler.commands.IncreaseCellCommand;

public class IncreaseCellCommandFactory extends ACommandFactory {
	public IncreaseCellCommandFactory(Memory memory) { super(memory); }

	@Override
	public ACommand CreateCommand() {
		return new IncreaseCellCommand(this.getMemory());
	}
}
