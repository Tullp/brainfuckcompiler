package compiler;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        while (!input.equals("exit")) {
            List<Character> code = input.chars().mapToObj(x -> (char)x).collect(Collectors.toList());
            Invoker invoker = new Parser(code, new Memory(System.out)).getInvoker(List::isEmpty);
            invoker.invoke();
            input = scanner.nextLine();
        }
    }
}
