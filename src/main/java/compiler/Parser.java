package compiler;

import compiler.commandParsers.ACommandParser;
import org.reflections.Reflections;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;

public class Parser {

	private final List<Character> code;
	private final Memory memory;

	private static Map<Character, Class<?>> getCommandParsersMap() {
		Map<Character, Class<?>> commandParsersMap = new HashMap<>();
		Reflections reflections = new Reflections(Main.class.getPackage().getName());
		Set<Class<?>> classes = reflections.getTypesAnnotatedWith(BrainfuckCommand.class);
		for (Class<?> cls : classes) commandParsersMap.put(cls.getAnnotation(BrainfuckCommand.class).character(), cls);
		return commandParsersMap;
	}

	public Parser(List<Character> code, Memory memory) {
		this.code = code;
		this.memory = memory;
	}

	public Invoker getInvoker(Predicate<List<Character>> stopFunc) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
		Map<Character, Class<?>> commandParsersMap = Parser.getCommandParsersMap();
		Invoker invoker = new Invoker();

		while (!stopFunc.test(this.code)) {
			ACommandParser commandParser = (ACommandParser) commandParsersMap.get(this.code.get(0))
				.getConstructor(Memory.class, List.class)
				.newInstance(this.memory, this.code);
			invoker.addCommand(commandParser.getCommandFactory().CreateCommand());
		}

		return invoker;
	}
}
