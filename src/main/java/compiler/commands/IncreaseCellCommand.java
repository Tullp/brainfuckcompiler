package compiler.commands;

import compiler.Memory;

public class IncreaseCellCommand extends ACommand {
	public IncreaseCellCommand(Memory memory) { super(memory); }

	@Override
	public void Execute() {
		this.getMemory().IncreaseCell();
	}
}
