package compiler.commands;

import compiler.Memory;

public class DecreaseCellCommand extends ACommand {
	public DecreaseCellCommand(Memory memory) { super(memory); }

	@Override
	public void Execute() {
		this.getMemory().DecreaseCell();
	}
}
