package compiler.commands;

import compiler.Memory;

public class IncreasePointerCommand extends ACommand {
	public IncreasePointerCommand(Memory memory) { super(memory); }

	@Override
	public void Execute() {
		this.getMemory().IncreasePointer();
	}
}
