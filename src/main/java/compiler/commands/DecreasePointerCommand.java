package compiler.commands;

import compiler.Memory;

public class DecreasePointerCommand extends ACommand {
	public DecreasePointerCommand(Memory memory) { super(memory); }

	@Override
	public void Execute() {
		this.getMemory().DecreasePointer();
	}
}
