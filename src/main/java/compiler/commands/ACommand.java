package compiler.commands;

import compiler.Memory;

public abstract class ACommand {
	private final Memory memory;
	public ACommand(Memory memory) {
		this.memory = memory;
	}
	public Memory getMemory() { return this.memory; }
	abstract public void Execute();
}
