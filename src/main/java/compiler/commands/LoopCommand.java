package compiler.commands;

import compiler.Invoker;
import compiler.Memory;

public class LoopCommand extends ACommand {

	private final Invoker invoker;

	public LoopCommand(Memory memory, Invoker invoker) {
		super(memory);
		this.invoker = invoker;
	}

	@Override
	public void Execute() {
		while (this.getMemory().GetCurrentCell() != 0)
			invoker.invoke();
	}
}
