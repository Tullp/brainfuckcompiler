package compiler.commands;

import compiler.Memory;

public class PrintCommand extends ACommand {
	public PrintCommand(Memory memory) {
		super(memory);
	}

	@Override
	public void Execute() {
		this.getMemory().Print();
	}
}
