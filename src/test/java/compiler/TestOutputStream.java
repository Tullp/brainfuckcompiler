package compiler;

import java.io.IOException;
import java.io.OutputStream;

class TestOutputStream extends OutputStream {

	private String data = "";

	public String getData() { return this.data; }

	@Override
	public void write(int b) throws IOException {
		this.data += (char)b;
	}
}
