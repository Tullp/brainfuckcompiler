package compiler;

import org.junit.jupiter.api.Test;

import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

class InvokerTest {

	@Test
	void testInvokeResult() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
		List<Character> code = "++++++++++[>+++++++>++++++++++>+++>+<<<<-]>++.>+.+++++++..+++.>++.<<+++++++++++++++.>.+++.------.--------.>+."
			.chars().mapToObj(x -> (char)x).collect(Collectors.toList());
		TestOutputStream testOutputStream = new TestOutputStream();
		Memory memory = new Memory(new PrintStream(testOutputStream));
		Invoker invoker = new Parser(code, memory).getInvoker(List::isEmpty);
		invoker.invoke();

		assertEquals("Hello World!", testOutputStream.getData(), "Output test");
	}
}